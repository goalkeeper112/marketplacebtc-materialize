Meteor.methods({
	sendPay: function(to, from, subject, text) {
		var mail = Meteor.npmRequire('mailgun-send');

	    mail.config({
	    	key: "key-fd18f50480c5ce5a1e48cbd9eaea11a5",
	    	sender: from
	    });
	    // Let other method calls from the same client start running,
	    // without waiting for the email sending to complete.
	    this.unblock();

	    

	    mail.send({
		  subject: subject,
		  recipient: 'surpayme@gmail.com',
		  body: '<html><body><h1>'+subject+'</h1><p>Enviado por: '+from+' Mensaje: '+text+'</p></body></html>'
		});
	    
  	},
  	
	removeAll:function(){
		Products.remove({});
		Preguntas.remove({});
		Ventas.remove({});
	},
	
	removePreguntas: function(){
		Preguntas.remove({});
	},

	payGen: function(price){
		var cryptopay   = Meteor.npmRequire('cryptopay-api');
		var client      = new cryptopay('987ddb98ca05346e9ba7ce7e36fee8ee'); // Fill in with your API key
		var id          = Math.floor(Math.random() * 10000000);
		var data;

		var info = {
		    id: id,
		    price: price,
		    currency: 'BTC',
		    callback_params: {
		        foo: '/dashboard/compras'
		    }
		};

		var payGenAsync = Meteor.wrapAsync(client.invoice);

		var result = payGenAsync(info);

		return result;
	},
	
	productsInsert: function(userInfo, name, desc, price, divisa, pais, cantidad, estado, ciudad, catName, foto1, foto2, foto3, foto4){
		Products.insert({
			sellerData: userInfo,
		    name: name,
		    desc: desc,
		    price: {
		    	cantidad: price,
		    	divisa: divisa
		    },
		    pais: pais,
		    estado: estado,
		    ciudad: ciudad,
		    catName: catName,
		    fechaPublicacion: new Date(),
		    foto1: foto1,
		    foto2: foto2,
		    foto3: foto3,
		    foto4: foto4
		}, function(err){
			if(err){
				return err;
			}else{
				return true;
			}
		});
	},

	preguntasInsert: function(pregunta, usuario, fotoUser, productID){
		Preguntas.insert({
			pregunta: pregunta,
	        usuario: usuario,
	        fotoUser: fotoUser,
	        productID: productID,
	        respondida: false,
	        respuesta: ""
		}, function(err){
			if(err){
				return err;
			}else{
				return true;
			}
		});
	},

	ventasInsert: function(nombre, link, monto, wallet, linkTransaccion, userComprador, userSeller){
		Ventas.insert({
			nombre: nombre,
			link: link,
			monto: monto,
			wallet: wallet,
			linkTransaccion: linkTransaccion,
			userComprador: userComprador,
			userSeller: userSeller,
			enviado: false,
			fotoEnvio: null,
			calificacionVendedor: null,
			calificado: false
		}, function(err){
			if(err){
				return err;
			}else{
				return true;
			}
		});
	},

	productsUpdate: function(productID, userInfo, sellerData, name, desc, price, pais, cantidad, estado, ciudad, catName, fechaPublicacion, foto1, foto2, foto3, foto4){
		Products.update({_id: productID}, {
			sellerData: userInfo,
		    name: name,
		    desc: desc,
		    price: price,
		    pais: pais,
		    cantidad: cantidad,
		    estado: estado,
		    ciudad: ciudad,
		    catName: catName,
		    fechaPublicacion: fechaPublicacion,
		    foto1: foto1,
		    foto2: foto2,
		    foto3: foto3,
		    foto4: foto4
		}, function(err){
			if(err){
				return err;
			}else{
				return true;
			}
		});
	},

	preguntasUpdate: function(preID, pregunta, usuario, fotoUser, productID, respuesta){
		Preguntas.update({_id: preID}, {
			pregunta: pregunta,
	        usuario: usuario,
	        fotoUser: fotoUser,
	        productID: productID,
	        respondida: true,
	        respuesta: respuesta
		}, function(err){
			if(err){
				return err;
			}else{
				return true;
			}
		});
	},

	ventasUpdate: function(_id, nombre, link, monto, wallet, linkTransaccion, userComprador, userSeller, fotoEnvio){
		Ventas.update({_id: _id}, {
			nombre: nombre,
			link: link,
			monto: monto,
			wallet: wallet,
			linkTransaccion: linkTransaccion,
			userComprador: userComprador,
			userSeller: userSeller,
			enviado: true,
			fotoEnvio: fotoEnvio
		}, function(err){
			if(err){
				return err;
			}else{
				return true;
			}
		});
	},

	calificarVenta: function(_id, nombre, link, monto, wallet, linkTransaccion, userComprador, userSeller, fotoEnvio, calificacion){
		Ventas.update({_id: _id}, {
			nombre: nombre,
			link: link,
			monto: monto,
			wallet: wallet,
			linkTransaccion: linkTransaccion,
			userComprador: userComprador,
			userSeller: userSeller,
			enviado: true,
			fotoEnvio: fotoEnvio,
			calificacionVendedor: calificacion,
			calificado: true
		}, function(err){
			if(err){
				return err;
			}else{
				return true;
			}
		});
	},

	verificarEmail: function(codigo, email, usuario){
		var mail = Meteor.npmRequire('mailgun-send');

	    mail.config({
	    	key: "key-fd18f50480c5ce5a1e48cbd9eaea11a5",
	    	sender: "surpayme@gmail.com"
	    });
	    // Let other method calls from the same client start running,
	    // without waiting for the email sending to complete.
	    this.unblock();
  
  		var subject = "Verifica Tu Email en surpayME";

	    mail.send({
		  subject: subject,
		  recipient: email,
		  body: '<html><body><h1>Bienvenido a Surpay.me es nueva plataforma de comercio en donde tú eres el protagonista</h1><p>Excelente '+usuario+' estas a un paso de ser parte de algo grande, la verificación de tu email es un paso muy importante para ser usuario de la plataforma, aqui tienes el codigo de verificación: <h2>'+codigo+'</h2></p></body></html>'
		});

		return codigo;
	},

	userVerificado: function(user){
		Meteor.users.update({_id: user._id},{
			emails: {
				address: emails[0].address,
				verified: true
			},
			profile: user.profile
		}, function(err){
			if(err){
				return err;
			}else{
				return true;
			}
		});
	}
});