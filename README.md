###Surpay.me es la nueva plataforma de comercio en donde tu eres el protagonista

#¿Qué es Surpay.me?

	Surpay.me es la nueva plataforma en donde cualquiera puede vender o comprar los productos que desea haciendo uso de la criptomoneda Bitcoin como su estandarte, nuestra visión principal es que cualquiera pueda integrar a bitcoin en su vida cotidiana sin ninguna complicación olvidándose así de las barreras que las monedas FIAT (dolar, euro, peso, etc) le imponen a los países hermanos y comience a hacer cosas geniales con sus ganancias obtenidas sin importar de donde se encuentren.

#¿Porqué  Surpay.me?

	El uso de este tipo de plataformas es cada vez más común en nuestra sociedad, en cada país existe una alternativa estableciendo actividades comerciales con la moneda en curso legal de la región. Estas han brindado a las personas comunes como tú o como yo la posibilidad de crear grandes comercios sin necesidad de un gran local, todo a la distancia de un click, pero, ¿Cómo hacer cuando las monedas físicas no son la manera más eficiente de negociar?

	Nosotros, en surpay.me queremos romper esa barrera entre las distintas fronteras promoviendo el comercio entre usuarios de distintos paises sin limitación alguna. 

	En nuestra plataforma nos olvidamos de las monedas FIAT y le ofrecemos una manera más segura de hacer negocios a nuestros clientes, el Bitcoin.

#¿Qué ofrecemos?

	* Una moderna plataforma
	* Diseño moderno y responsivo
	* Seguridad en todo momento
	* Sistema de Escrow en cada transacción
	* Aplicación Móvil para Android
	* Conexión con tus productos en cada momento
	* Plataforma Gratuita sin ningún costo de suscripción
	* Libre de publicidades que entorpecen la experiencia del usuario

#¿Qué no ofrecemos?

	* Una wallet para tus Bitcoins
	* Perfiles falsos que sirvan para estafa(cada usuario sera verificado)
	* Plan de suscripción 
	* No cobramos por registrarte ni comprar en ella
	* No somos un servicio freemium

#¿Cual es nuestro precio?

	Nuestra forma de monetizar es fija, solo cobramos un porcentaje aún no establecido por cada venta, esto debido a que usamos un sistema de escrow centralizado en donde nosotros somos los responsable de tú dinero.
