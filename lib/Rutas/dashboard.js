  Router.route('dashboard', {
    path: '/dashboard',
    template: 'dashboard'
  }, function(){
    return GAnalytics.pageview();
  });

  Router.route('dashboard/preguntas', {
    template: "preguntas",
    path: "/dashboard/preguntas",
    waitOn: function() { 
      Meteor.subscribe('productos');
      return Meteor.subscribe('preguntas'); 
    },
  }, function(){
    return GAnalytics.pageview();
  });

  Router.route('dashboard/compras', {
    template: "compras",
    path: "/dashboard/compras",
    waitOn: function() { 
      return Meteor.subscribe('ventas'); 
    },
  }, function(){
    return GAnalytics.pageview();
  });

  Router.route('dashboard/productos/publicados', {
    template: "publicados",
    path: "/dashboard/productos/publicados",
    waitOn: function() {
      Meteor.subscribe('imagenes');
      return Meteor.subscribe('productos'); 
    },
  }, function(){
    return GAnalytics.pageview();
  });

  Router.route('productos', {
    template: 'productos',
    path: "/dashboard/productos"
  }, function(){
    return GAnalytics.pageview();
  });

  Router.route('dashboard/productos/ventas', {
    template: "ventas",
    path: "/dashboard/productos/ventas",
    waitOn: function() { 
      return Meteor.subscribe('ventas'); 
    },
  }, function(){
    return GAnalytics.pageview();
  });

  Router.route('modificarProducto', {
    waitOn: function() {
      Meteor.subscribe('imagenes');
      Meteor.subscribe('paises');
      Meteor.subscribe('subcategories');
      Meteor.subscribe('categories');
      return Meteor.subscribe('productos'); 
    },
    template: "modificarProducto",
    path: "/dashboard/productos/modificarProducto/:_id"
  }, function(){
    return GAnalytics.pageview();
  });