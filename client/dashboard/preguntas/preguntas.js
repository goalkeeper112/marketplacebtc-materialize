Template.preguntas.helpers({
	preguntasRealizadas: function(){
		var preguntas = Preguntas.find({respondida: false});
		var productos = Products.find({ sellerData: Meteor.user()});
		var pre = [];
		preguntas.forEach(function(pregunta){
			productos.forEach(function(producto){
				if(producto._id === pregunta.productID){
					return pre.push(pregunta);
				}
			});
		});
		Session.set('pre', pre);
		return pre;
	}
});

Template.preguntas.events({
	'click #responder': function(){
		var url = Gravatar.imageUrl(Meteor.user().emails[0].address, {
	      size: 34,
	      default: 'mm'
	    });
		Meteor.call('preguntasUpdate', $('#preID').val(), $('#pregunta').val(), $('#usuario').val(), $('#fotoUser').val(), $('#productID').val(), $('#respuesta').val(), url, function(err){
			if(err){
				return noty({
					        position: 'topRight',
					        text: err,
					        animation: {
					            open: {height: 'toggle'}, // jQuery animate function property object
					            close: {height: 'toggle'}, // jQuery animate function property object
					            easing: 'swing', // easing
					            speed: 500 // opening & closing animation speed
					        }
					    });
			}else{
				return noty({
					        position: 'topRight',
					        text: 'Pregunta Respondida con exito',
					        animation: {
					            open: {height: 'toggle'}, // jQuery animate function property object
					            close: {height: 'toggle'}, // jQuery animate function property object
					            easing: 'swing', // easing
					            speed: 500 // opening & closing animation speed
					        }
					    });
			}
		});
	}
});