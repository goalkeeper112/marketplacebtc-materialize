Template.publicados.helpers({
	ventasRealizadas: function(){
		return Products.find({ sellerData: Meteor.user()});
	}
});

Template.publicados.events({
	'click #eliminar': function(){
		var eliminar = prompt("¿Desea eliminar este producto de manera definitiva? (si/no)");
		if(eliminar === "si"){
			Products.remove({ _id: this._id});
			return noty({
	          	position: 'topRight',
	          	text: 'Tu Producto ha sido eliminado con exito',
	          	animation: {
	            	  open: {height: 'toggle'}, // jQuery animate function property object
	            	  close: {height: 'toggle'}, // jQuery animate function property object
	            	  easing: 'swing', // easing
	        	      speed: 500 // opening & closing animation speed
	    	      }
		    });
		}else{
			return false;
		}
	},

	"click .modificar": function(){
		return Session.set('productID', this._id);
	}
});