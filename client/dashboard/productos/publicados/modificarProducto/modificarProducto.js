Template.modificarProducto.helpers({
	productoModify: function(){
		return Products.find({_id: Session.get('productID')});
	},
	subCategories: function(){
    	return SubCategories.find({});
  	},
 	paisOrigen: function(){
    	return Paises.find({});
  	}
});

Template.modificarProducto.events({
	"click .modifyProducto": function(){
		Meteor.call('productsUpdate', Session.get('productID'), Meteor.user(), $('#nombre').val(), $('#descripcion').val(), $('#precio').val(), $('#pais').val(), $('#estado').val(), $('#ciudad').val(), $('#catName').val(), $('#fechaPublicacion').val(), $('#foto1').val(), $('#foto2').val(), $('#foto3').val(), $('#foto4').val(), function(err){
			if(err){
				return noty({
					        position: 'topRight',
					        text: err,
					        animation: {
					            open: {height: 'toggle'}, // jQuery animate function property object
					            close: {height: 'toggle'}, // jQuery animate function property object
					            easing: 'swing', // easing
					            speed: 500 // opening & closing animation speed
					        }
					    });
			}else{
				return  noty({
					        position: 'topRight',
					        text: 'Tu producto ha sido modificado con exito!',
					        animation: {
					            open: {height: 'toggle'}, // jQuery animate function property object
					            close: {height: 'toggle'}, // jQuery animate function property object
					            easing: 'swing', // easing
					            speed: 500 // opening & closing animation speed
					        }
				    	});
			}
		});
    	return true;
	}
});