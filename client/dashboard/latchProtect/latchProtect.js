Session.setDefault('Meteor._latch.pairing', false);

Template.latchProtect.helpers({
  isPairing: function() {
    return Session.get('Meteor._latch.pairing');
  },
  isPaired: Latch.isPaired
});

Template.latchProtect.events({
  "click #latch-pair-link" : function (evt, tmpl) {
    Tracker.flush();
    var token = prompt("Abre tu aplicación de latch y selecciona 'Add a new service o Añadir un nuevo servicio'.\nEscribe aqui abajo tu codigo de token");
    if (!token) return;
    Session.set('Meteor._latch.pairing', true);
    Latch.pair(token, function(err, res) {
        Session.set('Meteor._latch.pairing', false);
        if (err) {
            return noty({
                      position: 'topRight',
                      text: "El token introducido es invalido",
                      animation: {
                          open: {height: 'toggle'}, // jQuery animate function property object
                          close: {height: 'toggle'}, // jQuery animate function property object
                          easing: 'swing', // easing
                          speed: 500 // opening & closing animation speed
                      }
                  });
        } else {
            return noty({
                      position: 'topRight',
                      text: 'Tu cuenta ha sido protegida con Latch!',
                      animation: {
                          open: {height: 'toggle'}, // jQuery animate function property object
                          close: {height: 'toggle'}, // jQuery animate function property object
                          easing: 'swing', // easing
                          speed: 500 // opening & closing animation speed
                      }
                  });
        }
    });
  },
  "click #latch-unpair-link" : function (evt, tmpl) {
    if (!confirm("¿Estas seguro que quieres deshabilitar la protección de latch en tu cuenta?")) return;

    Latch.unpair(function(err, res) {
        if (err) {
           return noty({
                      position: 'topRight',
                      text: err.reason,
                      animation: {
                          open: {height: 'toggle'}, // jQuery animate function property object
                          close: {height: 'toggle'}, // jQuery animate function property object
                          easing: 'swing', // easing
                          speed: 500 // opening & closing animation speed
                      }
                  });
        } else {
            return noty({
                      position: 'topRight',
                      text: "La protección de Latch en tu cuenta ha sido deshabilitada",
                      animation: {
                          open: {height: 'toggle'}, // jQuery animate function property object
                          close: {height: 'toggle'}, // jQuery animate function property object
                          easing: 'swing', // easing
                          speed: 500 // opening & closing animation speed
                      }
                  });
        }
    });
  }
});