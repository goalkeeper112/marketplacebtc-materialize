Session.set('calificar', null);
Template.compras.helpers({
	comprasRealizadas: function(){
		return Ventas.find({userComprador: Meteor.user()});;
	},
	compraEnviada: function(){
		if(Ventas.findOne({_id: this._id, userComprador: Meteor.user(), enviado: true})){
			return true;
		}else{
			return false;
		}
	},
	emailSeller: function(){
		return this.userSeller.emails[0].address;
	},

	calificarTmpl: function(){
		if(Session.get('calificar') === true){
			return true;
		}else{
			return false;
		}

		if (Session.get('calificar') != true) {
			Session.set('calificar', false);
			return false;
		}

		return Session.get('calificar');
	}
});

Template.compras.events({
	'click #btnCalificar': function(){
		Session.set('calificar', true);
	}
})