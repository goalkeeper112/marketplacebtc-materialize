Template.miniatura.helpers({
	priceTo: function(){

	      switch(this.price.divisa){
	        case "VEF":
	          var priceTo = this.price.cantidad / Session.get('pricesBTC').BTC.VEF;
	          return priceTo.toFixed(8);
	          break;

	        case "USD":
	          var priceTo = this.price.cantidad / Session.get('pricesBTC').BTC.USD;
	          return priceTo.toFixed(8);
	          break;

	        case "ARS":
	          var priceTo = this.price.cantidad / Session.get('pricesBTC').BTC.ARS;
	          return priceTo.toFixed(8);
	          break;
	      };

		return priceTo.toFixed(8);
  }
});