Template.comentarios.events({
  "click .responder": function(){
    Router.go('/producto/pregunta/responder');
    return Session.set('preguntaID', $('#preguntaID').val());
  }
});

Template.comentarios.helpers({
  'Responder': function(){
      if(Meteor.user().profile.alias === Products.findOne({ _id: Session.get('productID')}).dataUser.profile.alias){
        return true;
      }

      if(Preguntas.findOne({_id: $('preguntaID').val()}).respuesta.length >= 1){
        return false;
      }
  }
});
