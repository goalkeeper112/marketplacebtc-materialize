Template.comentarBox.events({
  'click #preguntar': function(){
    var url = Gravatar.imageUrl(Meteor.user().emails[0].address, {
      size: 34,
      default: 'mm'
    });
      Meteor.call('preguntasInsert', $('#pregunta').val(), $('#userPregunton').val(), url, Session.get('productID'), function(err){
        if(err){
          return noty({
                      position: 'topRight',
                      text: err,
                      animation: {
                          open: {height: 'toggle'}, // jQuery animate function property object
                          close: {height: 'toggle'}, // jQuery animate function property object
                          easing: 'swing', // easing
                          speed: 500 // opening & closing animation speed
                      }
                  });
        } else{
          $('#pregunta').val('');
          return noty({
              position: 'topRight',
              text: 'Pregunta realizada con exito, espera tu respuesta',
              animation: {
                  open: {height: 'toggle'}, // jQuery animate function property object
                  close: {height: 'toggle'}, // jQuery animate function property object
                  easing: 'swing', // easing
                  speed: 500 // opening & closing animation speed
              }
          });
        }
      });
  }
});
