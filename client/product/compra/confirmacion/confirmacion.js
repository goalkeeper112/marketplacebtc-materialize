Template.confirmacion.events({
	'click #si': function(){
		Meteor.call('payGen', Session.get('priceProduct'), function(err, data){
			Session.set('pay', data);
		});
		return Router.go('/compra/pago');
	},
	'click #no': function(){
		return Router.go('/');
	}
});
