Template.pago.events({
	'click #listo': function(){
		return Router.go('/compra/confirmPago');
	}
});

Template.pago.helpers({
	invoice: function(){
		return Session.get('pay').url;
	}
});