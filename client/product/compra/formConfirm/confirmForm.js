Template.confirmForm.events({
	'submit .formPay': function(){
		var to = "surpayme@gmail.com", 
			from = Meteor.users.findOne({ _id: $('#userComprador').val()}).emails[0].address, 
			subject = "Pago del producto: " + $('#nombre').val(), 
			text = "El usuario: "+Meteor.users.findOne({ _id: $('#userComprador').val()}).profile.username+" ha realizado la compra del producto "+$('#link').val()+" pagando un monto de "+$('#monto').val()+" desde la dirección bitcoin "+$('#wallet').val()+" dejando como dirección de transacción el siguiente link "+$('#linkTransaccion').val();
		Meteor.call('sendPay', to, from, subject, text, function(err){
			if(err){
				return noty({
					          position: 'topRight',
					          text: err,
					          animation: {
					              open: {height: 'toggle'}, // jQuery animate function property object
					              close: {height: 'toggle'}, // jQuery animate function property object
					              easing: 'swing', // easing
					              speed: 500 // opening & closing animation speed
					          }
					      });
			}
		});

		Meteor.call('ventasInsert', $('#nombre').val(), $('#link').val(), $('#monto').val(), $('#wallet').val(), $('#linkTransaccion').val(), Meteor.user(), Session.get('userSeller'), function(err){
			if(err){
				return noty({
			          position: 'topRight',
			          text: err,
			          animation: {
			              open: {height: 'toggle'}, // jQuery animate function property object
			              close: {height: 'toggle'}, // jQuery animate function property object
			              easing: 'swing', // easing
			              speed: 500 // opening & closing animation speed
			          }
			      });
			}else{
				return noty({
			          position: 'topRight',
			          text: 'Tu compra ya esta registrada en tu panel <a href="/dashboard/compras">Ir a compras</a>',
			          animation: {
			              open: {height: 'toggle'}, // jQuery animate function property object
			              close: {height: 'toggle'}, // jQuery animate function property object
			              easing: 'swing', // easing
			              speed: 500 // opening & closing animation speed
			          }
			      });
			}
		});

		Router.go('/');
		noty({
	          position: 'topRight',
	          text: 'Tu formulario de pago ha sido aceptado con exito',
	          animation: {
	              open: {height: 'toggle'}, // jQuery animate function property object
	              close: {height: 'toggle'}, // jQuery animate function property object
	              easing: 'swing', // easing
	              speed: 500 // opening & closing animation speed
	          }
	      });
		return false;
	}
});